/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rahul
 */
public class Chapter {
    private String name;
    private int subjectId;
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    private String created_at = format.format(date);
    
    public Chapter(String name, int subjectId){
        this.name = name;
        this.subjectId = subjectId;
    }
    
    public String getName(){
        return this.name;
    }
    
    public int getSubjectId(){
        return this.subjectId;
    }
    
    public String getCreatedDate(){
        return this.created_at;
    }
}
