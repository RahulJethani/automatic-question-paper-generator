/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Giris
 */
public class Subject {
    private String name;
    private String course;
    private int semester;
    private int user_id;
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    private String created_at = format.format(date);
    
    public Subject(String name, int semester, String course){
        this.name = name;
        this.semester = semester;
        this.course = course;
    }
    
    public void setUserId(int user_id){
        this.user_id = user_id;
    }
    
    public int user_id(){
        return this.user_id;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getCourse(){
        return this.course;
    }
    
    public int getSemester(){
        return this.semester;
    }
    
    public String getCreatedDate(){
        return this.created_at;
    }
}
