/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Giris
 */
public class Question {
    private String question;
    private int marks;
    private String difficultyLevel;
    private int chapterId;
    private int teacherId;
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
    private String created_at = format.format(date);
    private String updated_at = format.format(date);
    
    public Question(String question, int marks, String difficultyLevel, int chapterId, int teacherId){
        this.question = question;
        this.marks = marks;
        this.difficultyLevel = difficultyLevel;
        this.chapterId = chapterId;
        this.teacherId = teacherId;
    }
    
    public String getQuestionName(){
        return this.question;
    }
    
    public int getMarks(){
        return this.marks;
    }
    
    public String getDifficultyLevel(){
        return this.difficultyLevel;
    }
    
    public int getChapterId(){
        return this.chapterId;
    }
    
    public int getTeacherId(){
        return this.teacherId;
    }
    
    public String getCreatedDate(){
        return this.created_at;
    }
     
    public String getUpdatedDate(){
        return this.updated_at;
    }
    
}
