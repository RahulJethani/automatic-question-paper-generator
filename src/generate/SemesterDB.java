/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generate;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Giris
 */
public class SemesterDB {

    public void generateSemesterPaper(Map<String, Integer> questionPaper) {
        try {
            ArrayList<String> questions = new ArrayList<>(questionPaper.keySet());
            ArrayList<String> usedQuestions = new ArrayList<>();
            Random random = new Random();
            FileWriter fw = new FileWriter("Semester.txt",false);
            fw.write("\t\t\t\tSemester Paper \t\n\n");
            fw.write("\t\t\t\t\t\t\tTotal Marks : 80\n");
            fw.write("\t\t\t\tDuration: 3Hrs\n");
            fw.write("\tNB 1. Question is compulsory.\n\t  2. Attempt any three from the remaining five questions.\n\t  3. Figures to the right indicate full marks.\n\n\n");

            int marks = 0;
            int numOfQuesMax = 4;
            int i=1;
            int questionsPerQuestion = 1;
            int currentMarks = 0;
            while(i<7){
                fw.write("\n\nQ."+i+" Attempt All Questions: \n");
                while(marks<20){
                    String randomQuestion = questions.get(random.nextInt(questions.size()));
                    currentMarks = marks + questionPaper.get(randomQuestion);
                    if(usedQuestions.contains(randomQuestion)){
                        continue;
                    }
                    
                    if(currentMarks > 20){
                        if(marks <20){
                            continue;
                        }
                    }
                    
                    if(questionsPerQuestion == numOfQuesMax && currentMarks != 20){
                        continue;
                    }
                    
                    usedQuestions.add(randomQuestion);
                    fw.write(" "+questionsPerQuestion+". "+randomQuestion+" \t\t\t" + "(" + questionPaper.get(randomQuestion)+ "m )\n");
                    marks+= questionPaper.get(randomQuestion);
                    questionsPerQuestion++;
                    
                }
                questionsPerQuestion = 1;
                i++;
                marks = 0;
            }
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(SemesterDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
