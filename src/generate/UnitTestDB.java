/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generate;

import db.QuestionDB;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Giris
 */
public class UnitTestDB {
    public void randomTwoAndFourMarksQuestions(Map<String, Integer> questionPaper) {
        Map<String, Integer> twoMarksQuestions = new HashMap<>();
        Map<String, Integer> fourMarksQuestions = new HashMap<>();
        questionPaper.forEach((question, marks) -> {
            if(marks == 2){
                twoMarksQuestions.put(question, marks);
            }else{
                fourMarksQuestions.put(question, marks);
            }
        });
        
        Random random = new Random();
        List<String> keys = new ArrayList<String>(twoMarksQuestions.keySet());
        twoMarksQuestions.clear();
        while(twoMarksQuestions.size() < 6){
            String randomKey = keys.get(random.nextInt(keys.size()));
            twoMarksQuestions.put(randomKey, 2);
        }
        
        keys = new ArrayList<String>(fourMarksQuestions.keySet());
        fourMarksQuestions.clear();
        while(fourMarksQuestions.size() < 4){
            String randomKey = keys.get(random.nextInt(keys.size()));
            fourMarksQuestions.put(randomKey, 4);
        }
        
        this.saveUnitTestPaper(twoMarksQuestions, fourMarksQuestions);
    }

    protected void saveUnitTestPaper(Map<String, Integer> twoMarksQuestions, Map<String, Integer> fourMarksQuestions) {
        try {
            FileWriter fw = new FileWriter("UnitTest.txt",false);
            fw.write("\t\t\t\tUnit Test Paper \t\n\n");
            fw.write("Choice Based Qustions:\t\t\tTotal Marks : 20\n\n");
            fw.write("Q1. Attempt any Four: \n\n");
            int count = 1;
            for (Map.Entry<String,Integer> entry : twoMarksQuestions.entrySet()) {
		String str = count+". "+entry.getKey() + "\t\t\t (" + entry.getValue()+"m)\n";
                count++;
                fw.write(str);
            }
            
            count = 1;
            fw.write("\nQ2. Attempt any Three: \n\n");
            for (Map.Entry<String,Integer> entry : fourMarksQuestions.entrySet()) {
		String str = count+". "+entry.getKey() + "\t\t\t (" + entry.getValue()+"m)\n";
                count++;
                fw.write(str);
            }
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(QuestionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
